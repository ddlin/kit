package xdb

type Config struct {
	Dsn         string `yaml:"dsn"`         // DSN data source name
	MaxIdle     int    `yaml:"maxIdle"`     //  设置空闲连接池的最大连接数。
	MaxOpen     int    `yaml:"maxOpen"`     //  设置数据库的最大打开连接数。
	MaxLifetime int    `yaml:"maxLifetime"` //  设置连接可重用的最大时间。 单位s
	LogMode     bool   `yaml:"logMode"`

	//普罗米修斯监控配置
	DBName   string `yaml:"dbName"`
	PushAddr string `yaml:"pushAddr"`
	//RefreshInterval int    `yaml:"refreshInterval"`
}
