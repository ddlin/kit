package xtask

import (
	"context"
	"gitee.com/ddlin/kit/xlog"
	"gitee.com/ddlin/kit/xrds"
	"github.com/hibiken/asynq"
)

func NewAsynqClient(conf xrds.Config) (client *asynq.Client, cleanup func()) {
	client = asynq.NewClient(asynq.RedisClientOpt{
		Addr:     conf.Addr,
		Password: conf.Password,
		DB:       conf.DB,
	})
	cleanup = func() {
		xlog.S(context.Background()).Infow("NewAsynqClient-应用退出")
		err := client.Close()
		if err != nil {
			xlog.S(context.Background()).Infow("NewAsynqClient-应用退出err", "err", err)
		}
	}
	return client, cleanup
}
