package hclient

import (
	"github.com/dghubble/sling"
	"net/http"
)

type AdRateLimitDoer struct {
	Doer sling.Doer
}

func (l AdRateLimitDoer) Do(req *http.Request) (resp *http.Response, err error) {

	resp, err = l.Doer.Do(req)

	//xlog.S(req.Context()).Infow("AdRateLimitDoer--信息", "AdRateLimitDoer", "111111112222")

	return
}
